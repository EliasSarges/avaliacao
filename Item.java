public class Item{
  protected String title;
  protected String publisher;
  protected String ibsn;
  protected String price;

  public Item(String title, String publisher, String ibsn, String price){
    this.title = title;
    this.publisher = publisher;
    this.ibsn = ibsn;
    this.price = price;
  }

  public void display(){
    System.out.println("title: " + this.title);
    System.out.println("publisher: " + this.publisher);
    System.out.println("isbn: " + this.ibsn);
    System.out.println("price: " + this.price);
  }

}